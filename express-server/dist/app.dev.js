"use strict";

var express = require("express"),
    cors = require('cors');

var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, function () {
  return console.log("Server runnig on port 3000");
});
var ciudades = ["Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile", "Mexico DF", "Nueva York"];
app.get("/ciudades", function (req, res, next) {
  return res.json(ciudades.filter(function (c) {
    return c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1;
  }));
});
var misDestinos = [];
app.get("/my", function (req, res, next) {
  return res.json(misDestinos);
});
app.post("/my", function (req, res, next) {
  console.log(req.body);
  misDestinos.push(req.body.nuevo);
  res.json(misDestinos);
});