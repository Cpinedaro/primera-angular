"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.DestinoViajeEffects = exports.reducerDestinosViajes = exports.ElegidoFavoritoAction = exports.NuevoDestinoAction = exports.DestinosViajesActionTypes = exports.initializeDestinosViajesState = void 0;
var core_1 = require("@angular/core");
var effects_1 = require("@ngrx/effects");
var operators_1 = require("rxjs/operators");
exports.initializeDestinosViajesState = function () {
    return {
        items: [],
        loading: false,
        favorito: null
    };
};
// ACCIONES
var DestinosViajesActionTypes;
(function (DestinosViajesActionTypes) {
    DestinosViajesActionTypes["NUEVO_DESTINO"] = "[Destinos Viajes] Nuevo";
    DestinosViajesActionTypes["ELEGIDO_FAVORITO"] = "[Destinos Viajes] Favorito";
})(DestinosViajesActionTypes = exports.DestinosViajesActionTypes || (exports.DestinosViajesActionTypes = {}));
var NuevoDestinoAction = /** @class */ (function () {
    function NuevoDestinoAction(destino) {
        this.destino = destino;
        this.type = DestinosViajesActionTypes.NUEVO_DESTINO;
    }
    return NuevoDestinoAction;
}());
exports.NuevoDestinoAction = NuevoDestinoAction;
var ElegidoFavoritoAction = /** @class */ (function () {
    function ElegidoFavoritoAction(destino) {
        this.destino = destino;
        this.type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    }
    return ElegidoFavoritoAction;
}());
exports.ElegidoFavoritoAction = ElegidoFavoritoAction;
//REDUCERS
function reducerDestinosViajes(state, action) {
    switch (action.type) {
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return __assign(__assign({}, state), { items: __spreadArrays(state.items, [action.destino]) });
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(function (x) { return x.setSelected(false); });
            var fav = action.destino;
            fav.setSelected(true);
            return __assign(__assign({}, state), { favorito: fav });
        }
    }
    return state;
}
exports.reducerDestinosViajes = reducerDestinosViajes;
//EFFECTS
var DestinoViajeEffects = /** @class */ (function () {
    function DestinoViajeEffects(actions$) {
        this.actions$ = actions$;
        this.nuevoAgregado$ = this.actions$.pipe(effects_1.ofType(DestinosViajesActionTypes.NUEVO_DESTINO), operators_1.map(function (action) { return new ElegidoFavoritoAction(action.destino); }));
    }
    __decorate([
        effects_1.Effect()
    ], DestinoViajeEffects.prototype, "nuevoAgregado$");
    DestinoViajeEffects = __decorate([
        core_1.Injectable()
    ], DestinoViajeEffects);
    return DestinoViajeEffects;
}());
exports.DestinoViajeEffects = DestinoViajeEffects;
