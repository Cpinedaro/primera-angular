"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = exports.childrenRoutesVuelos = void 0;
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
//importando ruteo
var router_1 = require("@angular/router");
//importando formularios
var forms_1 = require("@angular/forms");
var store_1 = require("@ngrx/store");
var effects_1 = require("@ngrx/effects");
var app_component_1 = require("./app.component");
var destino_viaje_component_1 = require("./component/destino-viaje/destino-viaje.component");
var lista_destinos_component_1 = require("./component/lista-destinos/lista-destinos.component");
var destino_detalle_component_1 = require("./component/destino-detalle/destino-detalle.component");
var form_destino_viaje_component_1 = require("./component/form-destino-viaje/form-destino-viaje.component");
var destinos_api_client_model_1 = require("./models/destinos-api-client.model");
var destinos_viajes_state_model_1 = require("./models/destinos-viajes-state.model");
var login_component_1 = require("./components/login/login/login.component");
var protected_component_1 = require("./components/protected/protected/protected.component");
var usuario_logueado_guard_1 = require("./guards/usuario-logueado/usuario-logueado.guard");
var auth_service_1 = require("./services/auth.service");
var vuelos_component_component_1 = require("./components/vuelos/vuelos-component/vuelos-component.component");
var vuelos_main_component_component_1 = require("./components/vuelos/vuelos-main-component/vuelos-main-component.component");
var vuelos_mas_info_component_component_1 = require("./components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component");
var vuelos_detalle_component_component_1 = require("./components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component");
var reservas_module_1 = require("./reservas/reservas.module");
exports.childrenRoutesVuelos = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'main', component: vuelos_main_component_component_1.VuelosMainComponentComponent },
    { path: 'mas-info', component: vuelos_mas_info_component_component_1.VuelosMasInfoComponentComponent },
    { path: ':id', component: vuelos_detalle_component_component_1.VuelosDetalleComponentComponent },
];
var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: lista_destinos_component_1.ListaDestinosComponent },
    { path: 'destino/:id', component: destino_detalle_component_1.DestinoDetalleComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'protected', component: protected_component_1.ProtectedComponent, canActivate: [usuario_logueado_guard_1.UsuarioLogueadoGuard] },
    {
        path: 'vuelos',
        component: vuelos_component_component_1.VuelosComponentComponent,
        canActivate: [usuario_logueado_guard_1.UsuarioLogueadoGuard],
        children: exports.childrenRoutesVuelos
    }
];
var reducers = {
    destinos: destinos_viajes_state_model_1.reducerDestinosViajes
};
var reducersinitialState = {
    destinos: destinos_viajes_state_model_1.initializeDestinosViajesState()
};
// redux fin init
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                destino_viaje_component_1.DestinoViajeComponent,
                lista_destinos_component_1.ListaDestinosComponent,
                destino_detalle_component_1.DestinoDetalleComponent,
                form_destino_viaje_component_1.FormDestinoViajeComponent,
                login_component_1.LoginComponent,
                protected_component_1.ProtectedComponent,
                vuelos_component_component_1.VuelosComponentComponent,
                vuelos_main_component_component_1.VuelosMainComponentComponent,
                vuelos_mas_info_component_component_1.VuelosMasInfoComponentComponent,
                vuelos_detalle_component_component_1.VuelosDetalleComponentComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                router_1.RouterModule.forRoot(routes),
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                store_1.StoreModule.forRoot(reducers, { initialState: reducersinitialState }),
                effects_1.EffectsModule.forRoot([destinos_viajes_state_model_1.DestinoViajeEffects]),
                reservas_module_1.ReservasModule
            ],
            providers: [
                destinos_api_client_model_1.DestinosApiClient, auth_service_1.AuthService, usuario_logueado_guard_1.UsuarioLogueadoGuard
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
