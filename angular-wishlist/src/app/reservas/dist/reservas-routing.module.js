"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ReservasRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var reservas_detalle_component_1 = require("./reservas-detalle/reservas-detalle.component");
var reservas_listado_component_1 = require("./reservas-listado/reservas-listado.component");
var routes = [
    { path: 'reservas', component: reservas_listado_component_1.ReservasListadoComponent },
    { path: 'reservas/:id', component: reservas_detalle_component_1.ReservasDetalleComponent }
];
var ReservasRoutingModule = /** @class */ (function () {
    function ReservasRoutingModule() {
    }
    ReservasRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], ReservasRoutingModule);
    return ReservasRoutingModule;
}());
exports.ReservasRoutingModule = ReservasRoutingModule;
