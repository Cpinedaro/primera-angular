"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ReservasApiClientService = void 0;
var core_1 = require("@angular/core");
var ReservasApiClientService = /** @class */ (function () {
    function ReservasApiClientService() {
    }
    ReservasApiClientService.prototype.getAll = function () {
        return [{ id: 1, name: 'uno' }, { id: 2, name: 'dos' }];
    };
    ReservasApiClientService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ReservasApiClientService);
    return ReservasApiClientService;
}());
exports.ReservasApiClientService = ReservasApiClientService;
