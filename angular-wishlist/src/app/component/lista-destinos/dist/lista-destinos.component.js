"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ListaDestinosComponent = void 0;
var core_1 = require("@angular/core");
var destinos_viajes_state_model_1 = require("../../models/destinos-viajes-state.model");
var ListaDestinosComponent = /** @class */ (function () {
    function ListaDestinosComponent(destinosApiClient, store) {
        var _this = this;
        this.destinosApiClient = destinosApiClient;
        this.store = store;
        this.onItemAdded = new core_1.EventEmitter();
        this.updates = [];
        this.store.select(function (state) { return state.destinos.favorito; })
            .subscribe(function (d) {
            if (d = null) {
                _this.updates.push('Se ha elegido a' + d.nombre);
            }
        });
    }
    ;
    ListaDestinosComponent.prototype.ngOnInit = function () {
    };
    ListaDestinosComponent.prototype.agregado = function (d) {
        this.destinosApiClient.add(d);
        this.onItemAdded.emit(d);
        this.store.dispatch(new destinos_viajes_state_model_1.NuevoDestinoAction(d));
    };
    ListaDestinosComponent.prototype.elegido = function (e) {
        this.destinosApiClient.elegir(e);
        this.store.dispatch(new destinos_viajes_state_model_1.ElegidoFavoritoAction(e));
    };
    __decorate([
        core_1.Output()
    ], ListaDestinosComponent.prototype, "onItemAdded");
    ListaDestinosComponent = __decorate([
        core_1.Component({
            selector: 'app-lista-destinos',
            templateUrl: './lista-destinos.component.html',
            styleUrls: ['./lista-destinos.component.css']
        })
    ], ListaDestinosComponent);
    return ListaDestinosComponent;
}());
exports.ListaDestinosComponent = ListaDestinosComponent;
