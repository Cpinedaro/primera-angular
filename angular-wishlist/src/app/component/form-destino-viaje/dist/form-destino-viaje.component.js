"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FormDestinoViajeComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var destino_viaje_model_1 = require("../../models/destino-viaje.model");
var operators_1 = require("rxjs/operators");
var ajax_1 = require("rxjs/ajax");
var FormDestinoViajeComponent = /** @class */ (function () {
    function FormDestinoViajeComponent(fb) {
        this.fb = fb;
        this.minLongitud = 5;
        //inicializar
        this.onItemAdded = new core_1.EventEmitter();
        //vinculacion con tag html
        this.fg = this.fb.group({
            nombre: ['', forms_1.Validators.compose([
                    forms_1.Validators.required,
                ])],
            url: ['']
        });
        //observador de tipeo
        this.fg.valueChanges.subscribe(function (form) {
            console.log('cambio el formulario: ', form);
        });
    }
    FormDestinoViajeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var elemNombre = document.getElementById('nombre');
        rxjs_1.fromEvent(elemNombre, 'input')
            .pipe(operators_1.map(function (e) { return e.target.value; }), operators_1.filter(function (text) { return text.length > 2; }), operators_1.debounceTime(200), operators_1.distinctUntilChanged(), operators_1.switchMap(function () { return ajax_1.ajax('/assets/datos.json'); })).subscribe(function (AjaxResponse) {
            console.log(AjaxResponse);
            console.log(AjaxResponse.response);
            _this.searchResults = AjaxResponse.response;
        });
    };
    FormDestinoViajeComponent.prototype.guardar = function (nombre, url) {
        var d = new destino_viaje_model_1.DestinoViaje(nombre, url);
        this.onItemAdded.emit(d);
        return false;
    };
    __decorate([
        core_1.Output()
    ], FormDestinoViajeComponent.prototype, "onItemAdded");
    FormDestinoViajeComponent = __decorate([
        core_1.Component({
            selector: 'app-form-destino-viaje',
            templateUrl: './form-destino-viaje.component.html',
            styleUrls: ['./form-destino-viaje.component.css']
        })
    ], FormDestinoViajeComponent);
    return FormDestinoViajeComponent;
}());
exports.FormDestinoViajeComponent = FormDestinoViajeComponent;
