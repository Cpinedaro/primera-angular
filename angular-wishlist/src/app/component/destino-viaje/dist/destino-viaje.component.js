"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DestinoViajeComponent = void 0;
var core_1 = require("@angular/core");
var DestinoViajeComponent = /** @class */ (function () {
    function DestinoViajeComponent() {
        this.cssClass = "col-md-4";
        this.clicked = new core_1.EventEmitter();
    }
    DestinoViajeComponent.prototype.ngOnInit = function () {
    };
    DestinoViajeComponent.prototype.ir = function () {
        this.clicked.emit(this.destino);
        return false;
    };
    __decorate([
        core_1.Input()
    ], DestinoViajeComponent.prototype, "destino");
    __decorate([
        core_1.HostBinding("attr.class")
    ], DestinoViajeComponent.prototype, "cssClass");
    __decorate([
        core_1.Output()
    ], DestinoViajeComponent.prototype, "clicked");
    DestinoViajeComponent = __decorate([
        core_1.Component({
            selector: 'app-destino-viaje',
            templateUrl: './destino-viaje.component.html',
            styleUrls: ['./destino-viaje.component.css']
        })
    ], DestinoViajeComponent);
    return DestinoViajeComponent;
}());
exports.DestinoViajeComponent = DestinoViajeComponent;
